TODO
====
* Add garnishes to recipes that should have them. Many do not have those mentioned yet.
* Remove jitter during redraw - possilby caused by calling isotop forcefully during resize
* Make escape key clear the search
* Make filter button press clear search and re-do isotope search activation - currently re-pressing does nothing

NOTES
=====
To manage the yaml file, some handy tips::

    # Sort by cocktail name::
    cat data/cocktails/list.yaml | yq '.recipes | sort_by(.name)'

    # Title case for all names
    cat data/cocktails/list.yaml | sed -E 's/.*/\L&/g' | sed -e "/name/ s/\b\(.\)/\u\1/g" | sed 's/Name:/name:/g'

FILES OF INTEREST
=================
themes/custom-simple
    assets
        css
            cocktails.css   Cocktail style
        js
            cocktails.js    Cocktail code
            *.js            See <scripts> in baseof.html to find out which files are used
    data/cocktails
        list.yaml           Cocktail recipes
    layouts/_default
        baseof.html         The actual cocktail page
