//=== Redraw on resize of window, because isotope needs it to keep the layout coherent
window.onresize = function(){ debounce( function() {$grid.isotope();} ),200 }

//=== Capture escape button
$(document).on('keyup', function(e) {
  console.log("Key press detected:",e.key);
  if (e.key == "Escape") {
    console.log("ESCAPE!");
    $('#searchQueryInput').value="";
    $('#searchQueryInput').click();
  }
});

//=== Background image toggle
$('#c-toggleBackground').change(function(){
  if(this.checked) {
    $(".c-bg").css('opacity', 0);
  }
  else {
    $(".c-bg").css('opacity', 100);
  };
});

//=== Initialize isotope
var qsRegex;

var $grid = $('.c-card-group').isotope({
  itemSelector: '.c-card',
  layoutMode: 'fitRows',
  getSortData: {
    cocktail: ".c-name",
    ingredient: ".c-ingredient"
  },
  sortBy: [ 'cocktail' ]
});

//=== Search
var $quicksearch = $('#searchQueryInput').keyup( debounce( function() {
  qsRegex = new RegExp( $quicksearch.val(), 'gi' );
  $grid.isotope({  filter: function() { return qsRegex ? $(this).text().match( qsRegex ) : true; }});
}, 200 ) );

var $quicksearch = $('#searchQueryInput').click( function() {
  qsRegex = new RegExp( $quicksearch.val(), 'gi' );
  $grid.isotope({  filter: function() { return qsRegex ? $(this).text().match( qsRegex ) : true; }});
} );


function debounce( fn, threshold ) {
  var timeout;
  threshold = threshold || 100;
  return function debounced() {
    clearTimeout( timeout );
    var args = arguments;
    var _this = this;
    function delayed() {
      fn.apply( _this, args );
    }
    timeout = setTimeout( delayed, threshold );
  };
}

//=== Radio button filters
$("input[type='radio'][name='recipe-filter']").change(function() {
  var filterValue = $( this ).attr('data-string');
  filterValue = filterFns[ filterValue ] || filterValue;
  $grid.isotope({ filter: filterValue });
  $('#searchQueryInput').value="";
});

var filterFns = {
  gin: function() {
    var name = $(this).find('.c-ingredient').text().toLowerCase();
    console.log("Gin is here");
    return name.match( /gin/ );
  },
  whiskey: function() {
    var name = $(this).find('.c-ingredient').text().toLowerCase();
    return name.match( /whisk/ );
  },
  vodka: function() {
    var name = $(this).find('.c-ingredient').text().toLowerCase();
    return name.match( /vodka/ );
  },
  mezcal: function() {
    var name = $(this).find('.c-ingredient').text().toLowerCase();
    return name.match( /mezcal|tequila/ );
  },
  bitter: function() {
    var name = $(this).find('.c-ingredient').text().toLowerCase();
    return name.match( /campari|erol/ );
  },
  cognac: function() {
    var name = $(this).find('.c-ingredient').text().toLowerCase();
    return name.match( /cognac/ );
  },
  triplesec: function() {
    var name = $(this).find('.c-ingredient').text().toLowerCase();
    return name.match( /triple|coint|dramb|orange cura/ );
  },
  vermouth: function() {
    var name = $(this).find('.c-ingredient').text().toLowerCase();
    return name.match( /vermouth/ );
  },
  citrus: function() {
    var name = $(this).find('.c-ingredient').text().toLowerCase();
    return name.match( /(lemon|lime|grapefruit) juice/ );
  }
};